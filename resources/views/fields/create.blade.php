@extends('layouts.app')

@section('content')

	<div class="container">

		<h1>New Field</h1>
		<hr>

			{!! Form::open(['url' => 'fields']) !!}
				
				@include('fields.form', ['submitButtonText' => 'Add'])
				
			{!! Form::close() !!}

	</div>
	
@stop