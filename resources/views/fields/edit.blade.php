@extends('layouts.app')

@section('content')

	<div class="container">

		<h1>Editing: {!! $field->name !!}</h1>
		<hr>

			{!! Form::model($field, ['method' => 'PATCH', 'action' => ['FieldsController@update', $field->id]]) !!}
				
				@include('fields.form', ['submitButtonText' => 'Save changes'])

			{!! Form::close() !!}

	</div>

@stop
