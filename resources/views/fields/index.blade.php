@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">

			<div class="col-lg-8">
				<h1>Fields</h1>
			</div>

			<div class="col-lg-4">
				<a class="btn btn-success" href="{{ url('/fields/create') }}">Add field</a>
			</div>
			<hr>

			<table class="table">
				<thead>
					<th>id</th>
					<th width="300px">name</th>
					<th>Products amount</th>
					<th>Products</th>
					<th>Manage</th>
					<th></th>
				</thead>
				<tbody>

					@foreach($fields as $field)
						<tr>
							<td>{{ $field->id }}</td>
							<td>{{ $field->name }}</td>
							<td>{{ count($field->products) }}</td>
							<td>
								@foreach($field->products as $product)
									{{ $product->name }} ({{ $product->quantity }})<br>
								@endforeach
							</td>
							<td>
								<a class="btn-primary btn-sm" href="{{ route('products.index', [$field->id]) }}">Manage products</a>
							</td>
							<td>
								<a class="btn-primary btn-sm" href="{{ route('fields.edit', [$field->id]) }}">Edit</a>

								<form action="{{ route('fields.destroy', [$field->id]) }}" method="POST">
									@method('DELETE')
 									@csrf
 									<button type="submit" class="btn-danger btn-sm">Delete</button>  
								</form>
							</td>
						</tr>
					@endforeach

				</tbody>
			</table>	

			
		</div>
	</div>
@stop