@extends('layouts.app')

@section('content')

	<div class="container">

		<h1>New Product</h1>
		<hr>

			{{ Form::open(array('route' => array('products.store', $field->id), 'class' => 'section-top')) }}
				
				@include('products.form', ['submitButtonText' => 'Add'])
				
			{!! Form::close() !!}

	</div>
	
@stop