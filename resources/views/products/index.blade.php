@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">

			<div class="col-lg-8">
				<h1>Products of {{ $field->name }}</h1>
			</div>

			<div class="col-lg-4">
				<a class="btn btn-success" href="{{ route('products.create', ['field_id' => $field->id]) }}">Add product</a>
			</div>

			<hr>

			<table class="table">
				<thead>
					<th>id</th>
					<th width="300px">name</th>
					<th>Quantity</th>
					<th></th>
				</thead>
				<tbody>

					@foreach($products as $product)
						<tr>
							<td>{{ $product->id }}</td>
							<td>{{ $product->name }}</td>
							<td>{{ $product->quantity }}</td>
							<td>
								<a class="btn-primary btn-sm" href="{{ route('products.edit', ['field_id' => $field->id, 'product' => $product->id]) }}">Edit</a>

								<form action="{{ route('products.destroy', ['field_id' => $field->id, 'product' => $product->id]) }}" method="POST">
									@method('DELETE')
 									@csrf
 									<button type="submit" class="btn-danger btn-sm">Delete</button>  
								</form>
							</td>
						</tr>
					@endforeach

				</tbody>
			</table>	

			
		</div>
	</div>
@stop