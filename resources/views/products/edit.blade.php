@extends('layouts.app')

@section('content')

	<div class="container">

		<h1>Editing: {!! $product->name !!}</h1>
		<hr>


			{!! Form::model($product, ['method' => 'PATCH', 'action' => ['ProductsController@update', $field->id, $product->id]]) !!}
				
				@include('products.form', ['submitButtonText' => 'Save changes'])

			{!! Form::close() !!}

	</div>

@stop
