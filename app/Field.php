<?php

namespace App;

use Auth;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'name'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
