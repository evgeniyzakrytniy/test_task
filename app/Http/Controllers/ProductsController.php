<?php

namespace App\Http\Controllers;

use Auth;
use App\Field;
use App\Product;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($field_id, Request $request) {

        $field = Field::find($field_id);
        $products = $field->products;

    	return view('products.index', compact('field', 'products'));
    }

    public function create($field_id, Request $request) {

        $field = Field::find($field_id);

    	return view('products.create', compact('field'));
    }

    public function store($field_id, Request $request)  
    {

        $product = new Product($request->all());
        $product->save();

        $product->fields()->attach($field_id);

        return redirect('fields');
    
    }

    public function edit($field_id, $id) 
    {

        $product = Product::find($id);
        $field = Field::find($field_id);

        return view('products.edit', compact('product', 'field'));
    }

    public function update($field_id, $id, Request $request) 
    {

        $product = Product::find($id);
        $product->update($request->all());

        return redirect('fields');
    }

    public function destroy($field_id, $id) 
    {
        $product = Product::find($id);
        $product->delete();
        
        return redirect('fields');
    }
}
