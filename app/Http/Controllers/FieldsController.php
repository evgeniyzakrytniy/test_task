<?php

namespace App\Http\Controllers;

use Auth;
use App\Field;
use App\Product;

use Illuminate\Http\Request;

class FieldsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

    	$user = auth()->user();
    	$fields = $user->fields;

    	return view('fields.index', compact('user', 'fields'));
    }

    public function create() {

    	return view('fields.create');
    }

    public function store(Request $request)  
    {

        $field = new Field($request->all());
        $field->user_id = auth()->user()->id;

        $field->save();

        return redirect('fields');
    
    }

    public function edit($id) 
    {

        $field = Field::find($id);
        
        return view('fields.edit', compact('field'));
    }

    public function update($id, Request $request) 
    {

        $field = Field::find($id);
        $field->update($request->all());

        return redirect('fields');
    }

    public function destroy($id) 
    {
        $field = Field::find($id);
        $field->delete();
        
        return redirect('fields');
    }

}
