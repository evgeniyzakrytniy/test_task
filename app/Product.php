<?php

namespace App;

use Auth;
use App\Field;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'quantity'
    ];

    public function fields()
    {
        return $this->belongsToMany('App\Field');
    }
}
